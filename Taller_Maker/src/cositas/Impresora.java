package cositas;

public class Impresora {
	
	int avance = 0;
	int temperatura = 0; 
	String  tipo_material = "";
	String Modelo = "";
	float  voltaje = 120;
	
	
	public int get_avance(){
		return avance;
	}
	
	public void set_avance(int capaz){
	     avance = capaz;
	}
	
	public int get_temperatura(){
		return temperatura;
	}
	
	public void set_temperatura(int t){
	     this.temperatura = t;
	}
	
	
	public String get_tipo_material(){
		return tipo_material;
	}
	
	public void set_tipo_material(String tipo){
	     tipo_material= tipo ;
	}
	
	
	public String get_Modelo(){
		return Modelo;
	}
	
	public void set_Modelo(String M){
	     Modelo = M ;
	}

}
