package cositas;

public class Salon {
	float area;
	float voltaje;
	int personal;
	int mobiliario;
	int herramientas;
	int conectores;
	boolean status;
	String name;
	
	public Salon(){
		System.out.println("Hola a todos");
	}
	
	public boolean get_status(){
		return status;
	}
	
	public void set_status(boolean s){
		status=s;
	}
	
	public String get_name(){
		return name;
	}
	
	public void set_name(String mike){
	     name = mike ;
	}
	
	
	
	
	public int get_personal(){
		return personal;
	}
	
	public void set_personal(int p){
	     this.personal = p ;
	}
	
	
	
	public int get_mobiliario(){
		return mobiliario;
	}
	
	public void set_mobiliario(int m){
	     this.mobiliario = m ;
	}
	
	
	
	
	public int get_herramientas(){
		return herramientas;
	}
	
	public void set_herramientas(int h){
	     this.herramientas = h ;
	}
	

	public int get_conectores(){
		return conectores;
	}
	
	public void set_conectores(int c){
	     this.conectores = c ;
	}
	

	public float get_area(){
		return area;
	}
	
	public void set_area(float a){
	     this.area = a ;
	}
	

	public float get_voltaje(){
		return voltaje;
	}
	
	public void set_voltaje(float v){
	     this.voltaje = v ;
	}
	

	

}
